{
  global: {
    // User-defined global parameters; accessible to all component and environments, Ex:
    // replicas: 4,
  },
  components: {
    // Component-level parameters, defined initially from 'ks prototype use ...'
    // Each object below should correspond to a component in the components/ directory
    "phpdemo-ui": {
      containerPort: 80,
      image: "tim707/phpdemo:0.5",
      name: "ks-phpdemo-ui",
      replicas: 1,
      servicePort: 80,
      type: "LoadBalancer",
    },
  },
}
